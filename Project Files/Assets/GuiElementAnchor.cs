﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GuiElementAnchor : MonoBehaviour {


    public GameObject Source;
    public GameObject Target;
    Rect rect = new Rect(0, 0, 300, 100);
    Vector2 offset = new Vector2(0, 1.5f); // height above the target position
    // Use this for initialization
    void Start () {
        
    }
	
	// Update is called once per frame
	 void OnGUI() {
        Renderer renderer = Source.GetComponentInChildren<Renderer>();

        //Debug.Log(renderer==null?"null render":renderer.name);
        if (Source!= null && Source.activeInHierarchy && renderer!= null && renderer.enabled)
        {
            Target.SetActive(true);
            var point = Camera.main.WorldToScreenPoint(Source.transform.position + (Vector3)offset);
            rect.x = point.x;
            rect.y = Screen.height - point.y - rect.height; // bottom left corner set to the 3D point
            GUI.Label(rect, Source.name); // display its name, or other string

            Target.transform.position = point;


            //Debug.Log("Source is visible");
        }
        else
        {
            //Debug.log("source is not visible");
            Target.SetActive(false);

            

        }
    }
}
