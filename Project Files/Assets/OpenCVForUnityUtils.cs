﻿using OpenCvSharp;
using System.Threading.Tasks;
using UnityEngine;

namespace OpenCVForUnityUtils
{
    public static class Utils
    {
   

        public static Mat ToMat(this Texture2D texture)
        {
          
            Mat newMat = null;
            LongQianMethods.Utils.TextureToMat(ref texture, out newMat);

            return newMat;
           
        }

        public static Texture2D ToTexture2D(this Mat mat)
        {
            Texture2D text = null;
            LongQianMethods.Utils.MatToTexture(out text, ref mat);
            return text;
            
        }

    }

    namespace LongQianMethods
    {
        // Copyright (c) 2016, Long Qian
        // All rights reserved.
        //
        // Redistribution and use in source and binary forms, with or without modification, are
        // permitted provided that the following conditions are met:
        //
        //    * Redistributions of source code must retain the above copyright notice, this list
        //      of conditions and the following disclaimer.
        //    * Redistributions in binary form must reproduce the above copyright notice, this
        //      list of conditions and the following disclaimer in the documentation and/or other
        //      materials provided with the distribution.
        //
        // THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
        // EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
        // OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
        // SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
        // INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
        // PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
        // INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
        // STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
        // THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
        //
        public class Utils
        {
            static ParallelOptions parallelOptions = new ParallelOptions ();//{ MaxDegreeOfParallelism = -1 };

            // Convert Unity Texture2D object to OpenCVSharp Mat object
            public static void TextureToMat(ref Texture2D sourceTexture, out Mat videoSourceImage)
            {
                int imWidth = sourceTexture.width;
                int imHeight = sourceTexture.height;

                // Color32 array : r, g, b, a
                Color32[] c = sourceTexture.GetPixels32();
                Vec3b[] videoSourceImageData = new Vec3b[imHeight * imWidth];
                // Parallel for loop
                // convert Color32 object to Vec3b object
                // Vec3b is the representation of pixel for Mat

                Parallel.For(0, imHeight, parallelOptions, i => {
                    for (var j = 0; j < imWidth; j++)
                    {
                        var col = c[j + i * imWidth];
                        var vec3 = new Vec3b
                        {
                            Item0 = col.b,
                            Item1 = col.g,
                            Item2 = col.r
                        };
                        // set pixel to an array
                        videoSourceImageData[j + i * imWidth] = vec3;
                    }
                });
                // assign the Vec3b array to Mat
                videoSourceImage = new Mat(imHeight, imWidth, MatType.CV_8UC3);
                videoSourceImage.SetArray(0, 0, videoSourceImageData);
            }



            // Convert OpenCVSharp Mat object to Unity Texture2D object
            public static void MatToTexture(out Texture2D processedTexture, ref Mat image)
            {
                int imWidth = image.Cols;
                int imHeight = image.Rows;

                // cannyImageData is byte array, because canny image is grayscale
                Vec3b[] imageData =  new Vec3b[imHeight * imWidth];
                image.GetArray(0, 0, imageData);
                // create Color32 array that can be assigned to Texture2D directly
                Color32[] c = new Color32[imHeight * imWidth];

                // parallel for loop
                Parallel.For(0, imHeight, parallelOptions, i =>
                {
                    for (var j = 0; j < imWidth; j++)
                    {
                        Vec3b vec = imageData[j + i * imWidth];
                        var color32 = new Color32
                        {
                            r = vec.Item2,
                            g = vec.Item1,
                            b = vec.Item0,
                            a = 255
                        };
                        c[j + i * imWidth] = color32;
                    }
                });
                processedTexture = new Texture2D(imWidth, imHeight, TextureFormat.RGBA32, false, false);
                processedTexture.SetPixels32(c);
                // to update the texture, OpenGL manner
                processedTexture.Apply();
            }
        }

    }

}

