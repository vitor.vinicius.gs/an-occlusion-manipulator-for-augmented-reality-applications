﻿

//using UnityEngine;
using OpenCvSharp;
using System.Runtime;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;





public class HandHeadDetection /* : MonoBehaviour*/
{




    //const int FRAME_WIDTH = 640;    // Video frame
    //const int FRAME_HEIGHT = 360;   // Video fram size
    //int ismyframeuse = 0;
    //int palm_radius;
    //CascadeClassifier face_cascade;
    //Mat mybackground = null;
    //float radius_palm_center;
    bool showcondefects = true;
   // Point middle;
    Rect[] faces;
    Point center;

    //VideoCapture videocap;
    //Mat myframe;
    //int lengthMin;
    public int minAngle = -100;
    public int maxAngle = 200;
    public float boundingBoxHeightPerc = 0.15f;

    public int inAngleMax = 200;
    public int inAngleMin = 20;
    bool showmyhull = true;


    public bool UseRGB = true;
    public bool UseNormalizedRGB = true;
    public bool UseHSV = true;

    ParallelOptions parallelOptions = new ParallelOptions { MaxDegreeOfParallelism = -1 };

    SkinDetection SkinDetection = new SkinDetection();

    //mat2picture Mat2Bitmap;


    public OpenCvSharp.Rect facedetect(Mat frame, CascadeClassifier facecad)
    {
        Mat frame_gray = new Mat();
        Rect p;
        //Initialize
        p.Height = 0;
        p.Width = 0;
        p.X = 0;
        int maxarea = -1;
        int maxareai = -1;
        p.Y = 0;
        Cv2.CvtColor(frame, frame_gray, ColorConversionCodes.BGR2GRAY);
        Cv2.EqualizeHist(frame_gray, frame_gray);
        faces = facecad.DetectMultiScale(frame_gray, 1.1, 2, 0 | HaarDetectionType.ScaleImage, new Size(30, 30));
        for (int i = 0; i < faces.Length; i++) //consider the first face
        {
            Point center = new Point(faces[i].X + faces[i].Width * 0.5, faces[i].Y + faces[i].Height * 0.5);
            maxareai = (faces[i].Area() > maxarea) ? i : maxareai;
            maxarea = (faces[i].Area() > maxarea) ? faces[i].Area() : maxarea;



            Cv2.Ellipse(frame, center, new Size(faces[i].Width * 0.5, faces[i].Height * 0.5), 0, 0, 360, new Scalar(255, 0, 255), 4, LineTypes.Link8, 0);
        }
        if (faces.Length != 0) p = faces[maxareai];
        return p;
    }



    public void SkinColorModel(Mat frame, Rect faceregion, ref int ymax, ref int ymin, ref int crmax, ref int crmin, ref int cbmax, ref int cbmin)
    {
        
        int y, cb, cr, r, b, g, gray;
        Mat p = new Mat();

        Cv2.CvtColor(frame, p, ColorConversionCodes.BGR2YCrCb);
        

        crmax = -1;
        crmin = 295;
        cbmax = -1;
        cbmin = 295;
        ymax = 295;
        ymin = -1;


        if (faceregion.Area() > 5)
        {

            for (int i = faceregion.X; i < faceregion.X + faceregion.Width && i < frame.Cols; i++)
            {
                for (int j = faceregion.Y; j < faceregion.Y + faceregion.Height && j < frame.Rows; j++)
                {

                    b = frame.At<Vec3b>(j, i)[0];
                    g = frame.At<Vec3b>(j, i)[1];
                    r = frame.At<Vec3b>(j, i)[2];
                    y = p.At<Vec3b>(j, i)[0];
                    cr = p.At<Vec3b>(j, i)[1];
                    cb = p.At<Vec3b>(j, i)[2];
                    gray = (int)(0.2989 * r + 0.5870 * g + 0.1140 * b);
                    if (gray < 200 && gray > 40 && r > g && r > b)
                    {
                        ymax = (y > ymax) ? y : ymax;
                        ymin = (y < ymin) ? y : ymin;
                        crmax = (cr > crmax) ? cr : crmax;
                        crmin = (cr < crmin) ? cr : crmin;
                        cbmax = (cb > cbmax) ? cb : cbmax;
                        cbmin = (cb < cbmin) ? cb : cbmin;
                    }
                }
            }
            /**ymin = *ymin - 10;
			*ymax = *ymax + 10;
			*crmin = *crmin - 10;
			*crmax = *crmax + 10;
			*cbmin = *cbmin - 10;
			*cbmax = *cbmax + 10;*/
        }
        else
        {
            ymax = 255;//(*ymax>163) ? 163 : *ymax;
            ymin = 0;// (*ymin < 54) ? 54 : *ymin;
            crmax = 173;// (*crmax > 173) ? 173 : *crmax;
            crmin = 133;// (*crmin < 133) ? 133 : *crmin;
            cbmax = 127;// (*cbmax > 127) ? 127 : *cbmax;
            cbmin = 77;// (*cbmin < 77) ? 77 : *cbmin;
        }
        /**crmax = (*crmax > 173) ? 173 : *crmax;
		*crmin = (*crmin < 133) ? 133 : *crmin;
		*cbmax = (*cbmax > 127) ? 127 : *cbmax;
		*cbmin = (*cbmin < 77) ? 77 : *cbmin;*/
    }

    public void HandDetection(ref Mat frame, Rect faceregion, int ymax, int ymin, int crmax, int crmin, int cbmax, int cbmin, out Point[][] contours, out OpenCvSharp.Rect boundingBox, out Point center, out List<Point> fingertips, out List<Point> deptharr, out Point2f centerps,
    out float radips, bool draw = false )
    {
        int largest_area = 0;
        int largest_contour_index = 0;
        HierarchyIndex[] hierarchy;
        //cv::Size sz = cvGetSize(&frame);
        Mat mask = new Mat(frame.Rows, frame.Cols, MatType.CV_8U);
        Mat maskmat = mask;// new Mat(frame.Rows, frame.Cols, MatType.CV_8U);
        //Mat maskmat = new Mat(mask);

        if (faceregion.Area() > 5)
        {
            if (faceregion.Y > faceregion.Height / 4)
            {
                faceregion.Y -= faceregion.Height / 4;
                faceregion.Height += faceregion.Height / 4;
            }
            else
            {
                faceregion.Height += faceregion.Y;
                faceregion.Y = 0;

            }
            //avoid noise for T-shirt
            faceregion.Height += faceregion.Height / 2;
        }

        int y, cr, cb;
        //Turn to YCrCb
        Mat p = new Mat(), b = new Mat(), pyr = new Mat();
        Mat frameblur = new Mat();
        Cv2.Blur(frame, frameblur, new Size(5, 5));
        Cv2.CvtColor(frameblur, p, ColorConversionCodes.BGR2YCrCb);
        //Remove the face area & binary image
        for (int i = 0; i < frame.Cols; i++)
            for (int j = 0; j < frame.Rows; j++)
            {

                y = p.At<Vec3b>(j, i)[0];
                cr = p.At<Vec3b>(j, i)[1];
                cb = p.At<Vec3b>(j, i)[2];
                if (y > ymin && y < ymax && cr < crmax && cr > crmin && cb < cbmax && cb > cbmin)

                    maskmat.Set<byte>(j, i, 255);
                else maskmat.Set<byte>(j, i, 0);

                /*if (mybackground != NULL)
                {
                    b = mybackground;
                    if (abs((int)frame.at<cv::Vec3b>(j, i)[0] - (int)b.at<cv::Vec3b>(j, i)[0]) < 10 && abs((int)frame.at<cv::Vec3b>(j, i)[1] - (int)b.at<cv::Vec3b>(j, i)[1]) < 10 && abs((int)frame.at<cv::Vec3b>(j, i)[2] - (int)b.at<cv::Vec3b>(j, i)[2]) < 10)
                        maskmat.at<unsigned char>(j, i) = 0;
                }*/

            }
        if (faces != null)
            for (int i = 0; i < faces.Length; i++)
                for (int j = faces[i].X; j < faces[i].X + faces[i].Width; j++)
                    for (int k = faces[i].Y; k < faces[i].Y + faces[i].Height; k++)
                        maskmat.Set<byte>(k, j, 0);

        Mat erode_element = Cv2.GetStructuringElement(MorphShapes.Rect, new Size(3, 3));
        Mat dilate_element = Cv2.GetStructuringElement(MorphShapes.Rect, new Size(6, 6));
        //erode(mask, mask, erode_element); //ERODE first then DILATE to eliminate the noises.
        Cv2.Erode(mask, mask, erode_element);

        Cv2.Dilate(mask, mask, dilate_element);


        //Cv2.ImShow("mask", mask);


        Cv2.FindContours(mask, out contours, out hierarchy, RetrievalModes.External
            , ContourApproximationModes.ApproxSimple);

        //findContours(mask, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);
        /// Find the convex hull object for each contour
        Point[][] hull = new Point[contours.Length][];
        int[][] inthull = new int[contours.Length][];
        Vec4i[][] defects = new Vec4i[(contours.Length)][];

        //find largest contour
        for (int i = 0; i < contours.Length; i++) // iterate through each contour. 
        {
            double a = Cv2.ContourArea(contours[i], false);  //  Find the area of contour
            if (a > largest_area)
            {
                largest_area = (int)a;
                largest_contour_index = i;                //Store the index of largest contour
            }

        }

        //checkforcontourarea function if error occur
        if (contours.Length > 0)
        {

            //convexHull(Mat(contours[largest_contour_index]), hull[largest_contour_index], true);
            hull[largest_contour_index] = Cv2.ConvexHull(contours[largest_contour_index], true);

            inthull[largest_contour_index] = Cv2.ConvexHullIndices(contours[largest_contour_index], true);



            if (inthull[largest_contour_index].Length > 3) // If number of hull > 3  we will go to find the convexity defect

                defects[largest_contour_index] = Cv2.ConvexityDefects(contours[largest_contour_index], inthull[largest_contour_index]);

            if (!draw)
            {
                Cv2.DrawContours(frame, contours, largest_contour_index, new Scalar(0, 255, 0, 255), 1, LineTypes.Link8, hierarchy); // Draw the largest contour using previously stored index.																					  //draw hull as well
            }
            if (showmyhull)
                Cv2.DrawContours(frame, hull, largest_contour_index, Scalar.FromRgb(0, 255, 0), 2, LineTypes.Link8, hierarchy);
            boundingBox = Cv2.BoundingRect(hull[largest_contour_index]);

            if (showcondefects)
            {
                condefects(defects[largest_contour_index], contours[largest_contour_index], ref frame, boundingBox, out fingertips, out  deptharr, out centerps,
    out radips);
            }
            else
            {
                Mat tempMat = null;
                condefects(defects[largest_contour_index], contours[largest_contour_index], ref tempMat, boundingBox, out fingertips, out deptharr, out centerps,
    out radips);

            }
            Cv2.Rectangle(frame, boundingBox, new Scalar(255, 0, 0));
            center = new OpenCvSharp.Point(boundingBox.X + boundingBox.Width / 2, boundingBox.Y + boundingBox.Height / 2);

            Cv2.Ellipse(frame, center, new Size(20, 20), 360, 0, 360, new Scalar(2, 90, 255));
        }
        else {
            boundingBox = new Rect();
            center = new Point();
            fingertips = new List<Point>();
            centerps = new Point2f();
            radips = 0;
            deptharr = new List<Point>();
                }
    }


    public Mat HandDetection(Mat frame, out Point[][] contours, out OpenCvSharp.Rect boundingBox, out Point bCenter, out List<Point> fingertips, out List<Point> deptharr, out Point2f centerps,
    out float radips, bool draw = false)
    {

        int largest_area = 0;
        int largest_contour_index = 0;
        HierarchyIndex[] hierarchy;

        Mat mask = new Mat(frame.Rows, frame.Cols, MatType.CV_8U);
        //Mat maskmat = mask;// new Mat(frame.Rows, frame.Cols, MatType.CV_8U);
        

        //int y, cr, cb;
        //Turn to YCrCb
        //Mat p = new Mat();
        //Mat b = new Mat(), pyr = new Mat();
        
        Cv2.Blur(frame, frame, new Size(5, 5));
        //Cv2.CvtColor(frameblur, p, ColorConversionCodes.BGR2YCrCb);
       


        int imWidth = frame.Width;
        int imHeight = frame.Height;
        Vec3b[] imageData = new Vec3b[imHeight * imWidth];
        frame.GetArray(0, 0, imageData);
        


        byte[] videoSourceImageData = new byte[imHeight * imWidth];

        Parallel.For(0, imHeight, parallelOptions, i => {
            for (var j = 0; j < imWidth; j++)
            {
                var cor = imageData[j + i * imWidth];

                byte blue = cor.Item0;
                byte green = cor.Item1;
                byte red = cor.Item2;

                byte isSkin =  SkinDetection.ClassifySkin(red,green,blue,UseRGB,UseHSV,UseNormalizedRGB)? (byte)255 :(byte)0;
                
                // set pixel to an array
                videoSourceImageData[j + i * imWidth] = isSkin;
            }
        });
        // assign the Vec3b array to Mat
        mask.SetArray(0,0, videoSourceImageData);
        
        Mat erode_element = Cv2.GetStructuringElement(MorphShapes.Rect, new Size(3, 3));
        Mat dilate_element = Cv2.GetStructuringElement(MorphShapes.Rect, new Size(6, 6));
        //ERODE first then DILATE to eliminate the noises.
        Cv2.Erode(mask, mask, erode_element);

        Cv2.Dilate(mask, mask, dilate_element);
        
        Cv2.FindContours(mask, out contours, out hierarchy, RetrievalModes.External
           , ContourApproximationModes.ApproxNone);

        //findContours(mask, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);
        /// Find the convex hull object for each contour
        Point[][] hull = new Point[contours.Length][];
        int[][] inthull = new int[contours.Length][];
        Vec4i[][] defects = new Vec4i[(contours.Length)][];

        //find largest contour
        for (int i = 0; i < contours.Length; i++) // iterate through each contour. 
        {
            double a = Cv2.ContourArea(contours[i], false);  //  Find the area of contour
            if (a > largest_area)
            {
                largest_area = (int)a;
                largest_contour_index = i;                //Store the index of largest contour
            }

        }

        //checkforcontourarea function if error occur
        if (contours.Length > 0)
        {

            //convexHull(Mat(contours[largest_contour_index]), hull[largest_contour_index], true);
            hull[largest_contour_index] = Cv2.ConvexHull(contours[largest_contour_index], true);

            inthull[largest_contour_index] = Cv2.ConvexHullIndices(contours[largest_contour_index], true);

            if (inthull[largest_contour_index].Length > 3)
            { // If number of hull > 3  we will go to find the convexity defect

                defects[largest_contour_index] = Cv2.ConvexityDefects(contours[largest_contour_index], inthull[largest_contour_index]);
            }
            if (draw)
            {
                Cv2.DrawContours(frame, contours, largest_contour_index, new Scalar(0, 255, 0, 255), 1, LineTypes.Link8, hierarchy); // Draw the largest contour using previously stored index.																					  //draw hull as well
            }
            for(int i=0; i<hull.Length; i++)
            {
                if(hull[i] == null)
                {
                    hull[i] = new Point[] { };
                }
            }

            if (showmyhull && draw)
                Cv2.DrawContours(frame, hull, largest_contour_index, Scalar.FromRgb(0, 255, 0), 2, LineTypes.Link8, hierarchy);
            boundingBox = Cv2.BoundingRect(hull[largest_contour_index]);


            if (showcondefects && draw)
            {
                condefects(defects[largest_contour_index], contours[largest_contour_index], ref frame, boundingBox, out fingertips, out deptharr, out centerps,
    out radips);
            }
            else
            {
                Mat tempMat = null;
                condefects(defects[largest_contour_index], contours[largest_contour_index], ref tempMat, boundingBox, out fingertips, out  deptharr, out  centerps,
    out  radips);

            }
            
            if(draw)
                Cv2.Rectangle(frame, boundingBox, new Scalar(255, 0, 0));
            bCenter= center = new OpenCvSharp.Point(boundingBox.X + boundingBox.Width / 2, boundingBox.Y + boundingBox.Height / 2);
            if (draw)
                Cv2.Ellipse(frame, center, new Size(20, 20), 360, 0, 360, new Scalar(2, 90, 255));
        }else
        {
            boundingBox = new Rect();
            bCenter = new Point();
            fingertips = new List<Point>();
            centerps = new Point2f();
            radips = 0;
            deptharr = new List<Point>();
        }
        return frame;
    }
    private double innerAngle(double px1, double py1, double px2, double py2, double cx1, double cy1)
    {

        double dist1 = Math.Sqrt((px1 - cx1) * (px1 - cx1) + (py1 - cy1) * (py1 - cy1));
        double dist2 = Math.Sqrt((px2 - cx1) * (px2 - cx1) + (py2 - cy1) * (py2 - cy1));

        double Ax, Ay;
        double Bx, By;
        double Cx, Cy;
        Cx = cx1;
        Cy = cy1;
        if (dist1 < dist2)
        {
            Bx = px1;
            By = py1;
            Ax = px2;
            Ay = py2;


        }
        else
        {
            Bx = px2;
            By = py2;
            Ax = px1;
            Ay = py1;
        }


        double Q1 = Cx - Ax;
        double Q2 = Cy - Ay;
        double P1 = Bx - Ax;
        double P2 = By - Ay;


        double A = Math.Acos((P1 * Q1 + P2 * Q2) / (Math.Sqrt(P1 * P1 + P2 * P2) * Math.Sqrt(Q1 * Q1 + Q2 * Q2)));

        A = A * 180 / Cv2.PI;

        return A;
    }

    private List<Point> condefects(Vec4i[] convexityDefectsSet, Point[] mycontour, ref Mat frame, Rect boundingBox, out List<Point> fingertips, out List<Point> deptharr, out Point2f centerps,
    out float radips)
    {
        //Point2f mycenter;
        //float myradius;
        //float dis;
         deptharr = new List<Point>();
        fingertips = new List<Point>();


        for (int cDefIt = 0; cDefIt < convexityDefectsSet.Length; cDefIt++)
        {


            int startIdx = convexityDefectsSet[cDefIt].Item0;//[0];
            Point ptStart = new Point(mycontour[startIdx].X, mycontour[startIdx].Y);

            int endIdx = convexityDefectsSet[cDefIt].Item1; ;//[1];
            Point ptEnd = new Point(mycontour[endIdx].X, mycontour[endIdx].Y);

            int farIdx = convexityDefectsSet[cDefIt].Item2;//[2];
            Point ptFar = new Point(mycontour[farIdx].X, mycontour[farIdx].Y);

            double angle = Math.Atan2(center.Y - ptStart.Y, center.X - ptStart.X) * 180 / Cv2.PI;
            double inAngle = innerAngle(ptStart.X, ptStart.Y, ptEnd.X, ptEnd.Y, ptFar.X, ptFar.Y);
            double length = Math.Sqrt(Math.Pow(ptStart.X - ptFar.X, 2) + Math.Pow(ptStart.Y - ptFar.Y, 2));
            double centerLength = Math.Sqrt(Math.Pow(ptStart.X - center.X, 2) + Math.Pow(ptStart.Y - center.Y * 1.2, 2));
            double depth = (convexityDefectsSet[cDefIt].Item3) / 255;//antes tava 256
            
            if (angle > minAngle && angle < maxAngle && Math.Abs(inAngle) > inAngleMin && Math.Abs(inAngle) < inAngleMax && length > boundingBoxHeightPerc * boundingBox.Height)
            {
                fingertips.Add(ptStart);
                
            }


            //if (depth > 0 && depth < 120 && std::abs(inAngle) > 20 && std::abs(inAngle) < 120 && angle > -160 && angle < 160 && centerLength > lengthMin/100*boundingBox.height)
            //{
            //	line(frame, cv::Point(center.x / 1.1, center.y*1.2), ptStart,Scalar(0,255,0),1,8);
            //	circle(frame, ptStart, 20, Scalar(0, 255, 0), 1,8);
            //	fingertips.push_back(ptStart);
            //}
            if (depth > 10 && depth < 120)
            {
                var color = new Scalar(100, 0, 255);
                if(frame!=null)
                    Cv2.Circle(frame, center, 3, color, 2, LineTypes.Link8);
                //Cv2.Circle(frame, ptFar, 3, Scalar(100, 0, 255), 2, 8);


                deptharr.Add(ptFar);
            }
           

        }
        
        //if (deptharr.Count > 3)
        {
            Cv2.MinEnclosingCircle(deptharr, out centerps, out radips);
            //cv::minEnclosingCircle(deptharr, centerps, radips);
            if (frame != null)
                Cv2.Circle(frame, (int)centerps.X, (int)centerps.Y,(int) radips, new Scalar(0, 255, 0), 1, LineTypes.Link8);
        }

        for (int i = 0; i < fingertips.Count; i++)
        {
            if (deptharr.Count > 3)
            {
                if (frame != null)
                    Cv2.Line(frame, centerps, fingertips[i], new Scalar(0, 255, 0), 1, LineTypes.Link8);
            }
            if (frame != null)
                Cv2.Circle(frame, fingertips[i], 3, new Scalar(100, 0, 255), 5);
        }
        var colorDepth = new Scalar(100, 0, 255);
        for (int i =0; i < deptharr.Count; i++)
        {
            Cv2.Circle(frame, deptharr[i], 3, colorDepth, 2, LineTypes.Link8);
        }

        //if (fingertips.Count == 1)
        //{
        //    Cv2.PutText(frame, "01", new Point(400, 50), HersheyFonts.HersheyPlain, 2, new Scalar(255, 0, 0), 2, LineTypes.Link8);
        //}
        //else if (fingertips.Count == 2)
        //{
        //    Cv2.PutText(frame, "02", new Point(400, 50), HersheyFonts.HersheyPlain, 2, new Scalar(255, 0, 0), 2, LineTypes.Link8);
        //}
        //else if (fingertips.Count == 3)
        //{
        //    Cv2.PutText(frame, "03", new Point(400, 50), HersheyFonts.HersheyPlain, 2, new Scalar(255, 0, 0), 2, LineTypes.Link8);
        //}
        //else if (fingertips.Count == 4)
        //{
        //    Cv2.PutText(frame, "04", new Point(400, 50), HersheyFonts.HersheyPlain, 2, new Scalar(255, 0, 0), 2, LineTypes.Link8);
        //}
        //else if (fingertips.Count == 5)
        //{
        //    Cv2.PutText(frame, "05", new Point(400, 50), HersheyFonts.HersheyPlain, 2, new Scalar(255, 0, 0), 2, LineTypes.Link8);
        //}
        //work will start from here
        //if (krokaam)
        //workOnDefects(frame, fingertips, deptharr);

        return fingertips;

    }// condefects ends here



}
