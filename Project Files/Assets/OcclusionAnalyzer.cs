﻿using System.Collections.Generic;
using UnityEngine;
using System;
using System.Threading.Tasks;


public class OcclusionAnalyzer : MonoBehaviour {

    public bool active = true;

	// 4k = 3840 x 2160   1080p = 1920 x 1080
	public int captureWidth = 1920;
	public int captureHeight = 1080;
	ScreenRecorder.Format ARCameraFormat = ScreenRecorder.Format.JPG;
	public Renderer SourceRenderer = null;
	// folder to write output (defaults to data path)
	public string folder;

	public long MinSkinPixelCount  = 1000;
    UnityEngine.UI.RawImage spriteRenderer = null;
	public Camera ARSpaceCamera = null;
	public Camera ARCamera = null;
	public GameObject OverlaySprite = null;
	Color32 transparentPixel = new Color32 (1, 1, 1, 0);
	SkinDetection skinDetector = null;
	Vector2 pivot =new Vector2 (0f, 0f);
	Rect guiLoggerRect = new Rect (Screen.width/2-50, Screen.height/2-25, 100, 50);
	long countSkinPixel = 0;
	ScreenRecorder ArSpaceRecorder = null;
	ScreenRecorder ARRecorder = null;
    //public bool SaveImages = false;
    
    RectTransform spriteRecTran = null;

 //   void OnGUI(){
	//	//GUI.Label (guiLoggerRect, "Skin Pixels: "+  countSkinPixel);

	//}
    public int CustomKernelParameterKernelSize = -1;
    public float CustomKernelParameterKernelDiv = 1;
    public int CustomKernelParameterInteractions = -1;
    public bool CustomKernelParameterIsNormalized = false;

    List<float[][]> Kernels = new List<float[][]> {
        TextureFilter.EDGEDETECT_KERNEL_1,
        TextureFilter.EDGEDETECT_KERNEL_2,
        TextureFilter.EDGEDETECT_KERNEL_3,
        TextureFilter.EDGEDETECT_KERNEL_HORIZONTAL,
        TextureFilter.EDGEDETECT_KERNEL_VERTICAL,
        TextureFilter.SHARPEN_KERNEL,
        TextureFilter.LINEAR_KERNEL,
        TextureFilter.LINEAR_2_2_KERNEL_by4,
        TextureFilter.LINEAR_3_3_KERNEL_by9,
        TextureFilter.LINEAR_5_5_KERNEL_by25,
        TextureFilter.GAUSSIAN_KERNEL_3,
        TextureFilter.GAUSSIAN_KERNEL_5,
        TextureFilter.GAUSSIAN_KERNEL_7,
        TextureFilter.GAUSSIAN_KERNEL_9,

    };
    public enum KernelEnum {

        EDGEDETECT_KERNEL_1,
        EDGEDETECT_KERNEL_2,
        EDGEDETECT_KERNEL_3,
        EDGEDETECT_KERNEL_HORIZONTAL,
        EDGEDETECT_KERNEL_VERTICAL,
        SHARPEN_KERNEL,
        LINEAR_KERNEL,
        LINEAR_2_2_KERNEL_by4,
        LINEAR_3_3_KERNEL_by9,
        LINEAR_5_5_KERNEL_by25,
        GAUSSIAN_KERNEL_3,
        GAUSSIAN_KERNEL_5,
        GAUSSIAN_KERNEL_7,
        GAUSSIAN_KERNEL_9,
        CUSTOM_LINEAR,
        NONE
    };
    public KernelEnum Kernel = KernelEnum.LINEAR_2_2_KERNEL_by4;
    // Use this for initialization
    void Start () {
		spriteRenderer = OverlaySprite.GetComponent<UnityEngine.UI.RawImage> ();
        spriteRecTran = OverlaySprite.GetComponent<RectTransform>();


        skinDetector = new SkinDetection ();

		ArSpaceRecorder = new ScreenRecorder () {
			Camera = ARSpaceCamera,
			captureWidth = this.captureWidth,
			captureHeight = this.captureHeight,
			folder = this.folder,
			format = ScreenRecorder.Format.PNG
		};

		ARRecorder = new ScreenRecorder () {
			Camera = ARCamera,
			captureWidth = this.captureWidth,
			captureHeight = this.captureHeight,
			folder = this.folder,
			format = this.ARCameraFormat
		};



    }
	// Update is called once per frame
	Texture2D textureARCamera = null;
	RenderTexture renderTextureARCamera = null;
	Texture2D textureARSpaceCamera = null;
	RenderTexture renderTextureARSpaceCamera = null;


	void DetectOcclusion ()
	{

		if (textureARCamera != null) {
			Destroy (textureARCamera);
            textureARCamera = null;
        }
		if (textureARSpaceCamera != null) {
			Destroy (textureARSpaceCamera);
            textureARSpaceCamera = null;

        }
        if (active && (SourceRenderer != null && SourceRenderer.enabled && SourceRenderer.isVisible))
        {
            if (ARSpaceCamera != null && ARCamera != null)
            {

                //Passo 1: captura a imagens das cameras em camadas
                //long name = -1;
                ARRecorder.captureWidth = ArSpaceRecorder.captureWidth = this.captureWidth;
                ARRecorder.captureHeight = ArSpaceRecorder.captureHeight = this.captureHeight;


                ARRecorder.Capture(out textureARCamera, out renderTextureARCamera);



                ArSpaceRecorder.Capture(out textureARSpaceCamera, out renderTextureARSpaceCamera);
                

                spriteRecTran.sizeDelta = new Vector2(this.captureWidth, this.captureHeight);

                //Passo 2 e 3: verifica e classifica pixels de pele

                var pixelARCameraPixels = textureARCamera.GetPixels32();
                countSkinPixel = 0;
                var textureARSpaceCameraPixels = textureARSpaceCamera.GetPixels32();
                Parallel.For(0, captureWidth * captureHeight, new ParallelOptions { MaxDegreeOfParallelism = -1 }, (i) =>
                {

                    var pixel = textureARSpaceCameraPixels[i];

                    if (!(pixel.a == 0 && pixel.b == 0 && pixel.g == 0 && pixel.r == 0))
                    {
                        Color pixelARCamera = pixelARCameraPixels[i]; 
                        textureARSpaceCameraPixels[i] = pixelARCamera;

                        if (!skinDetector.ClassifySkin(pixelARCamera))
                        {
                            textureARSpaceCameraPixels[i] = transparentPixel;
                        }
                        else
                        {
                            countSkinPixel++;
                        }

                    }
                });


                renderTextureARCamera.DiscardContents();
                renderTextureARSpaceCamera.DiscardContents();
                Destroy(textureARSpaceCamera);
                textureARSpaceCamera = new Texture2D(captureWidth, captureHeight);
                textureARSpaceCamera.SetPixels32(textureARSpaceCameraPixels);
                if ( Kernel <= KernelEnum.GAUSSIAN_KERNEL_9)
                {
                    
                    var texture = TextureFilter.Convolution(textureARSpaceCamera, Kernels[(int)Kernel], 1);
                    Destroy(textureARSpaceCamera);
                    textureARSpaceCamera = null;
                    textureARSpaceCamera = texture;
                }
                else
                {

                    if(Kernel == KernelEnum.CUSTOM_LINEAR)
                    {
                        var texture = TextureFilter.Convolution(textureARSpaceCamera, TextureFilter.LinearKernel(CustomKernelParameterKernelSize, CustomKernelParameterIsNormalized, CustomKernelParameterKernelDiv), CustomKernelParameterInteractions);
                        Destroy(textureARSpaceCamera);
                        textureARSpaceCamera = null;
                        textureARSpaceCamera = texture;

                    }
                }

                
                textureARSpaceCamera.Apply();
                
                if (spriteRenderer.texture != null)
                {
                    Destroy(spriteRenderer.texture);
                }
                if (countSkinPixel > MinSkinPixelCount)
                {
                    spriteRenderer.texture = textureARSpaceCamera;
                    spriteRenderer.enabled = true;
                }
                else
                {
                    spriteRenderer.enabled = false;
                    spriteRenderer.texture = null;

                }

                
                //Passo n: libera os recursos ultilizados
                //limpa a textura e renderer do ARCamera
                Destroy(renderTextureARCamera);
                Destroy(textureARCamera);
                renderTextureARCamera.DiscardContents();
                renderTextureARCamera = null;
                textureARCamera = null;
                //limpa a textura e renderer do ARSpaceCamera
                Destroy(renderTextureARSpaceCamera);
                //deixa pra apagar na proxima interação
                //Destroy (textureARSpaceCamera);
                renderTextureARSpaceCamera.DiscardContents();
                renderTextureARSpaceCamera = null;
                //textureARSpaceCamera= null;//deixa pra apagar na proxima interação
                GC.Collect();

                
            }
        }
        else
        {
            spriteRenderer.enabled = false;
        }
       
	}


	void Update () {
		DetectOcclusion ();
	}

}

public class SkinDetection{

	float Min(float r, float g, float b)
	{
		var list = new float[] { r, g, b };
		float min = list[0];
		foreach (var item in list) {
			if (item < min) {
				min = item;
			}
		}
		return min;
	}
	float Max(float r, float g, float b)
	{
		var list = new float[] { r, g, b };
		float max = list[0];
		foreach (var item in list) {
			if (item > max) {
				max = item;
			}
		}
		return max;
	}

	public bool ClassifySkin(Color color)
	{
		byte r =(byte) (color.r * 255);
		byte g = (byte)(color.g * 255);
		byte b = (byte)(color.b * 255);

		var rgbClassifier = ((r > 95) && (g > 40 && g < 100) && (b > 20) && ((Max(r, g, b) - Min(r, g, b)) > 15) && (Math.Abs(r - g) > 15) && (r > g) && (r > b));
		var nurgb = ToNormalizedRGB(color);
		float nr = nurgb[0];
		float ng = nurgb[1];
		//float nb = nurgb[2];//nao utilizado
		var normRgbClassifier = (((nr / ng) > 1.185) && (((r * b) / (Math.Pow(r + g + b, 2))) > 0.107) && (((r * g) / (Math.Pow(r + g + b, 2))) > 0.112));
		var hsv = toHsvTest(color);
		var h = hsv[0];
		var s = hsv[1];
		var hsvClassifier = (h > 0 && h < 35 && s > 0.23 && s < 0.68);

        

		return (rgbClassifier || normRgbClassifier || hsvClassifier); //
	}

    public bool ClassifySkin(byte r, byte g, byte b,bool useRgb = true, bool useHsv = true, bool useNormalization = true)
    {
        Color color = new Color(r, g, b);

        bool normRgbClassifier = false;
        bool hsvClassifier = false;
        bool rgbClassifier = false;

        if (useRgb)
        {
             rgbClassifier = ((r > 95) && (g > 40 && g < 100) && (b > 20) && ((Max(r, g, b) - Min(r, g, b)) > 15) && (Math.Abs(r - g) > 15) && (r > g) && (r > b));
        }


        if (useNormalization)
        {
            var nurgb = ToNormalizedRGB(color);
            float nr = nurgb[0];
            float ng = nurgb[1];
            //float nb = nurgb[2];//nao utilizado
            normRgbClassifier = (((nr / ng) > 1.185) && (((r * b) / (Math.Pow(r + g + b, 2))) > 0.107) && (((r * g) / (Math.Pow(r + g + b, 2))) > 0.112));
        }
        if (useHsv)
        {
            var hsv =  ToHSV(color) ; //toHsvTest(color);
            var h = hsv[0];
            var s = hsv[1];
            hsvClassifier = (h > 0 && h < 35 && s > 0.23 && s < 0.68);
        }


        return (rgbClassifier || normRgbClassifier || hsvClassifier); //
    }

    private float[] ToHSV(Color color)
    {
        float hue;
        float saturation;
        float lightness;

        Color.RGBToHSV(color, out hue, out saturation, out lightness);

        return new float[] { hue, saturation, lightness };
    }

    float[] ToNormalizedRGB(Color color)
	{
		float soma = color.r + color.b + color.g;
		float r = (color.r / soma);
		float g = color.g / soma;
		float b = color.b / soma;

		return new float[] { r, g, b };

	}

	float[] toHsvTest(Color color)
	{
		float r = color.r* 255;
		float g = color.g* 255;
		float b = color.b* 255;
		float h = 0;
		var mx = Max(r, g, b);
		var mn = Min(r, g, b);
		var dif = mx - mn;

		if (mx == r)
		{
			h = (g - b) / dif;
		}
		else if (mx == g)
		{
			h = 2 + ((g - r) / dif);

		}
		else
		{
			h = 4 + ((r - g) / dif);
		}
		h = h * 60;
		if (h < 0)
		{
			h = h + 360;
		}

		return new float[] { h, 1 - (3 * ((Min(r, g, b)) / (r + g + b))), (1 / 3) * (r + g + b) };

	}


}
