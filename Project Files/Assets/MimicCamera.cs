﻿using UnityEngine;

public class MimicCamera : MonoBehaviour {

	// Use this for initialization
	public Camera SourceCamera = null;
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		var cam = GetComponent<Camera> ();

		cam.transform.SetPositionAndRotation (SourceCamera.transform.position, SourceCamera.transform.rotation);

        cam.fieldOfView = SourceCamera.fieldOfView;
        cam.farClipPlane = SourceCamera.farClipPlane;
        cam.nearClipPlane = SourceCamera.nearClipPlane;




        
        //using (new Window("src image", src))
        //using (new Window("dst image", dst))
        //{
        //    Cv2.WaitKey();
        //}


    }
}
