﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public static class OpenCVUtils
{
    public static int Area(this OpenCvSharp.Rect source)
    {
        return source.Width * source.Height;
    }
}

