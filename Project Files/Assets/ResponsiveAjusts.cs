﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResponsiveAjusts : MonoBehaviour {
    // Use this for initialization
    RectTransform recTra;
    void Start () {
        recTra = GetComponent<RectTransform>();

    }
	
	// Update is called once per frame
	void Update () {
        
        recTra.sizeDelta = new Vector2(Screen.width, Screen.height);
    }
}
