﻿using OpenCVForUnityUtils;
using OpenCvSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class Testes : MonoBehaviour
{
    ScreenRecorder ArSpaceRecorder = null;
    private ScreenRecorder MarkerRecorder;
    public Camera ARSpaceCamera = null;
    bool error = false;
    ScreenRecorder ARRecorder = null;
    public Camera ARCamera = null;
    public string folder;
    public int captureWidth = 1920;
    public int captureHeight = 1080;
    public UnityEngine.UI.Text DebugOutText = null;
    ScreenRecorder.Format ARCameraFormat = ScreenRecorder.Format.JPG;


    Texture2D textureARSpaceCamera = null;
    RenderTexture renderTextureARSpaceCamera = null;
    Texture2D textureARCamera = null;
    RenderTexture renderTextureARCamera = null;
    public UnityEngine.UI.RawImage TargetImage = null;
    SkinDetection skinDetection = new SkinDetection();
    public Camera MarkerCamera = null;
    private Texture2D textureMarkerCamera;
    private RenderTexture renderTextureMarkerCamera;

    public CascadeClassifier cascadeClassifier { get; private set; }

    void Start()
    {
        ARRecorder = new ScreenRecorder()
        {
            Camera = ARCamera,
            captureWidth = this.captureWidth,
            captureHeight = this.captureHeight,
            folder = this.folder,
            format = this.ARCameraFormat
        };

        ArSpaceRecorder = new ScreenRecorder()
        {
            Camera = ARSpaceCamera,
            captureWidth = this.captureWidth,
            captureHeight = this.captureHeight,
            folder = this.folder,
            format = ScreenRecorder.Format.PNG
        };

        MarkerRecorder = new ScreenRecorder()
        {
            Camera = MarkerCamera,
            captureWidth = this.captureWidth,
            captureHeight = this.captureHeight,
            folder = this.folder,
            format = ScreenRecorder.Format.PNG
        };



    }
    float time = 0;
    void Update()
    {

        time = (long)( Time.realtimeSinceStartup*1000);
        try
        {
            if (textureARCamera != null)
            {
                if (TargetImage.texture != null)
                    Destroy(TargetImage.texture);
                Destroy(textureARCamera);
                textureARCamera = null;
                renderTextureARCamera.DiscardContents();
                Destroy(renderTextureARCamera);
                renderTextureARCamera = null;
            }

            if (textureARSpaceCamera != null)
            {
                Destroy(textureARSpaceCamera);
            }
            textureARSpaceCamera = null;

            if (renderTextureARSpaceCamera != null)
            {
                renderTextureARSpaceCamera.DiscardContents();
                Destroy(renderTextureARSpaceCamera);
                renderTextureARSpaceCamera = null;
            }

            if (textureMarkerCamera != null)
            {
                Destroy(textureMarkerCamera);
            }
            textureMarkerCamera = null;
            if (renderTextureMarkerCamera != null)
            {
                renderTextureMarkerCamera.DiscardContents();
                Destroy(renderTextureMarkerCamera);
                renderTextureMarkerCamera = null;
            }
            ARRecorder.Capture(out textureARCamera, out renderTextureARCamera);
            ArSpaceRecorder.Capture(out textureARSpaceCamera, out renderTextureARSpaceCamera);

            MarkerRecorder.Capture(out textureMarkerCamera, out renderTextureMarkerCamera);

            using (Mat mat1 = textureARCamera.ToMat())
            {
                
                using (Mat mat = mat1.Blur(new Size(5, 5)))
                {

                    //Cv2.Blur(mat, mat, new Size(5, 5));



                    Mat mask = new Mat(mat.Rows, mat.Cols, MatType.CV_8U);


                    //Mat maskmat = mask;// new Mat(frame.Rows, frame.Cols, MatType.CV_8U);


                    //int y, cr, cb;
                    //Turn to YCrCb
                    //Mat p = new Mat();
                    //Mat b = new Mat(), pyr = new Mat();
                    //Mat frameblur = new Mat();
                    //Cv2.Blur(mat, frameblur, new Size(5, 5));
                    //Cv2.CvtColor(frameblur, p, ColorConversionCodes.BGR2YCrCb);



                    int imWidth = mat.Width;
                    int imHeight = mat.Height;
                    Vec3b[] imageData = new Vec3b[imHeight * imWidth];
                    mat.GetArray(0, 0, imageData);



                    byte[] videoSourceImageData = new byte[imHeight * imWidth];

                    Parallel.For(0, imHeight, new ParallelOptions { MaxDegreeOfParallelism = -1 }, i =>
                    {
                        for (var j = 0; j < imWidth; j++)
                        {
                            var cor = imageData[j + i * imWidth];

                            byte blue = cor.Item0;
                            byte green = cor.Item1;
                            byte red = cor.Item2;

                            byte isSkin = skinDetection.ClassifySkin(red, green, blue, true, true, true) ? (byte)255 : (byte)0;

                            // set pixel to an array
                            videoSourceImageData[j + i * imWidth] = isSkin;
                        }
                    });
                    mask.SetArray(0, 0, videoSourceImageData);


                    Mat erode_element = Cv2.GetStructuringElement(MorphShapes.Rect, new Size(3, 3));
                    Mat dilate_element = Cv2.GetStructuringElement(MorphShapes.Rect, new Size(6, 6));

                    mask = mask.Erode(erode_element);
                    mask = mask.Dilate(dilate_element);
                    Cv2.ImShow("DEBUG-MAO-SEGMENTADA", mask);
                    // get our unique filename
                    string filename = uniqueFilename((int)mask.Cols, (int)mask.Rows, "MAO-SEGMENTADA");
                    mask.SaveImage(filename, new ImageEncodingParam[] { new ImageEncodingParam(ImwriteFlags.JpegQuality, 70) });
                    var mat2 = textureMarkerCamera.ToMat();
                    Cv2.ImShow("DEBUG-MARCADOR", mat2);
                    filename = uniqueFilename((int)mask.Cols, (int)mask.Rows, "MARCADOR");
                    mat2.SaveImage(filename, new ImageEncodingParam[] { new ImageEncodingParam(ImwriteFlags.JpegQuality, 70) });
                    var mat3 = textureARSpaceCamera.ToMat();
                    Cv2.ImShow("DEBUG-OBJETO-VIRTUAL", mat3);
                    filename = uniqueFilename((int)mask.Cols, (int)mask.Rows, "OBJETO-VIRTUAL");
                    mat3.SaveImage(filename, new ImageEncodingParam[] { new ImageEncodingParam(ImwriteFlags.JpegQuality, 70) });
                    var mat4 = textureARCamera.ToMat();

                    var mat5 = mat4.Clone();
                    var mat6 = mat4.Clone();
                    for (int x = 0; x < mat4.Cols; x++)
                    {
                        for (int y = 0; y < mat4.Rows; y++)
                        {
                            
                            var color1 = mask.At<byte>(y, x);
                            var color2 = mat2.At<Vec3b>(y, x);
                            
                            var color4 = mat4.At<Vec3b>(y, x);
                           

                            
                            if (color2.Item0 != 0)
                            {
                                if (color1 == 255)
                                {
                                    mat4.Set<Vec3b>(y, x, new Vec3b(0, 0, 0));
                                }
                                
                            }
                            else
                            {
                                mat4.Set<Vec3b>(y, x, new Vec3b(0, 0, 0));
                            }



                        }
                    }


                    //Mat[] channels = mat4.Split();//split source  

                    //var histB = ObterHistograma(channels[0],true);
                    //var histG = ObterHistograma(channels[1], true);
                    //var histR = ObterHistograma(channels[2], true);


                    Cv2.ImShow("DEBUG-CAMERA-FILTRADA-SEM-MAO", mat4);
                    filename = uniqueFilename((int)mask.Cols, (int)mask.Rows, "CAMERA-FILTRADA-SEM-MAO");
                    mat4.SaveImage(filename, new ImageEncodingParam[] { new ImageEncodingParam(ImwriteFlags.JpegQuality, 70) });
                    Vec3b anterior = new Vec3b() { Item0 = 0, Item1 = 0, Item2 = 0 };
                    Vec3b mediaLocal = new Vec3b() { Item0 = 0, Item1 = 0, Item2 = 0 };

                    for (int x = 0; x < mat4.Cols; x++)
                    {
                        for (int y = 0; y < mat4.Rows; y++)
                        {

                            var color1 = mask.At<byte>(y, x);
                            var color2 = mat2.At<Vec3b>(y, x);

                            var color4 = mat4.At<Vec3b>(y, x);
                            
                            

                            if (color2.Item0 != 0)
                            {

                                mediaLocal = new Vec3b((byte)Math.Round((double)((color4[0] + anterior[0]) / 2)), (byte)Math.Round((double)((color4[1] + anterior[1]) / 2)), (byte)Math.Round((double)((color4[2] + anterior[2]) / 2)));

                                if (color1 == 255)
                                {

                                    Vec3b newColor = new Vec3b()
                                    {
                                        Item0 =  (byte)((color2[0] + mediaLocal[0]) / 2),
                                        Item1 =  (byte)((color2[1] + mediaLocal[1]) / 2),
                                        Item2 =  (byte)((color2[2] + mediaLocal[2]) / 2)

                                    };


                                    //mat2.Set<Vec3b>(y, x, newColor);

                                    mat4.Set<Vec3b>(y, x, newColor);
                                }



                                color4 = mat4.At<Vec3b>(y, x);
                                mat5.Set<Vec3b>(y, x, color4);
                            }

                            anterior = color4;

                        }
                    }

                    Cv2.ImShow("DEBUG-CAMERA-RECONSTRUIDA", mat4);
                    filename = uniqueFilename((int)mask.Cols, (int)mask.Rows, "CAMERA-RECONSTRUIDA");
                    mat4.SaveImage(filename, new ImageEncodingParam[] { new ImageEncodingParam(ImwriteFlags.JpegQuality, 70) });
                    Cv2.ImShow("DEBUG-FUNDO+RECONTRUIDA", mat5);
                    filename = uniqueFilename((int)mask.Cols, (int)mask.Rows, "FUNDO+RECONTRUIDA");
                    mat5.SaveImage(filename, new ImageEncodingParam[] { new ImageEncodingParam(ImwriteFlags.JpegQuality, 70) });
                    Cv2.ImShow("DEBUG-CAMERA-ORIGINAL", mat6);
                    filename = uniqueFilename((int)mask.Cols, (int)mask.Rows, "CAMERA-ORIGINAL");
                    mat6.SaveImage(filename, new ImageEncodingParam[] { new ImageEncodingParam(ImwriteFlags.JpegQuality, 70) });

                    int t = 10;
                    for (int x = 0; x < mat4.Cols; x++)
                    {
                        for (int y = 0; y < mat4.Rows; y++)
                        {

                            var color6 = mat6.At<Vec3b>(y, x);
                            var color5 = mat5.At<Vec3b>(y, x);

                            var b = color6[0] - color5[0];
                            var g = color6[1] - color5[1];
                            var r = color6[2] - color5[2];


                            if(!(b >= t && g >= t && r >= t))
                            {
                                mat6.Set<Vec3b>(y, x, color5);
                            }

                        }
                    }


                    //Mat final = (mat6 - mat5).ToMat();
                    Cv2.ImShow("DEBUG-RESULTADO-FINAL", mat6);
                    filename = uniqueFilename((int)mask.Cols, (int)mask.Rows, "RESULTADO-FINAL");
                    mat6.SaveImage(filename, new ImageEncodingParam[] { new ImageEncodingParam(ImwriteFlags.JpegQuality, 70) });
                    if (TargetImage != null)
                    {

                        using (var multiChannelImg = mask.CvtColor(ColorConversionCodes.GRAY2RGB))
                        {
                            Texture2D text = multiChannelImg.ToTexture2D();

                            TargetImage.texture = text;
                            TargetImage.enabled = true;
                        }
                    }

                }

            }
        }
        catch (Exception ex)
        {
            DebugOutText.text = "err[1]:" + ex.ToString();
            error = true;
        }

    }


    private int counter = 0;
    // create a unique filename using a one-up variable
    private string uniqueFilename(int width, int height, string prefix)
    {
        // if folder not specified by now use a good default
        if (folder == null || folder.Length == 0)
        {
            folder = Application.dataPath;
            if (Application.isEditor)
            {
                // put screenshots in folder above asset path so unity doesn't index the files
                var stringPath = folder + "/..";
                folder = Path.GetFullPath(stringPath);
            }
            folder += "/amostras";

            // make sure directoroy exists
            System.IO.Directory.CreateDirectory(folder);

            // count number of files of specified format in folder
            string mask = string.Format("{3}_{0}x{1}*.{2}", width, height, ".jpg", time + "_" + prefix);
            counter = Directory.GetFiles(folder, mask, SearchOption.TopDirectoryOnly).Length;
        }

        // use width, height, and counter for unique file name
        var filename = string.Format("{0}/{5}_{1}x{2}_{3}.{4}", folder, width, height, counter, ".jpg", time+"_"+prefix);

        // up counter for next call
        ++counter;

        // return unique filename
        return filename;
    }


    int[] ObterHistograma(Mat greyImage, bool normalizado = false)
    {
        int[] histograma = new int[256];
        for (int i = 0; i < 256; i++) histograma[i] = 0;

        int nivel = 0;
        for (int x = 0; x < greyImage.Rows; x++)
        {
            for (int y = 0; y < greyImage.Cols; y++)
            {
                nivel = greyImage.At<byte>(x, y);
                histograma[nivel] += 1;
            }
        }
        if (!normalizado) return histograma;

        int maior = 0;
        for (int i = 0; i < 256; i++)
        {
            if (histograma[i] > maior) maior = histograma[i];
        }

        int[] hn = new int[256];
        for (int i = 0; i < 256; i++)
        {
            hn[i] = (int)Math.Round((decimal)(histograma[i] * 255 / maior));
        }
        return hn;
    }


    Mat EqualizarHistogramaHSV(Mat image, Mat imagemDestino)
    {
        Mat imagemHSV = new Mat();
        //converte uma imagem RGB para HSV
        Cv2.CvtColor(image, imagemHSV, ColorConversionCodes.BGR2HSV);
        Mat[] planosHSV;
        //divide a imagem HSV em 3 planos de pixels
        Cv2.Split(imagemHSV, out planosHSV);
        //split(imagemHSV, planosHSV);
        //obtem apenas o plano H
        Mat H = planosHSV[2];
        int[] histograma = new int[256];
        for (int i = 0; i < 256; i++) histograma[i] = 0;

        int nivel = 0;
        for (int x = 0; x < H.Rows; x++)
        {
            for (int y = 0; y < H.Cols; y++)
            {
                nivel = H.At<byte>(x, y);
                histograma[nivel] += 1;
            }
        }
        //percorre apenas o plano H

        var area = H.Rows * H.Cols;
        decimal[] p = histograma.Select(x => ((decimal)x) / area).ToArray();

        decimal[] pa = new decimal[256];
        for (int i = 0; i < 256; i++)
        {
            decimal amount = 0;
            for (int j = 0; j <= i; j++)
            {
                amount += p[j];
            }
            pa[i] = amount;
        }
        int[] s = new int[256];
        for (int i = 0; i < 256; i++)
        {
            s[i] = (int)Math.Round((255 * pa[i]));
        }


        imagemHSV.Dispose();
        imagemHSV = new Mat();
        //converte uma imagem RGB para HSV
        Cv2.CvtColor(imagemDestino, imagemHSV, ColorConversionCodes.BGR2HSV);
        planosHSV = null;
        //divide a imagem HSV em 3 planos de pixels
        Cv2.Split(imagemHSV, out planosHSV);
        //split(imagemHSV, planosHSV);
        //obtem apenas o plano H
        H = planosHSV[2];



        for (int x = 0; x < H.Rows; x++)
        {
            for (int y = 0; y < H.Cols; y++)
            {
                byte pixel = H.At<byte>(x, y);
                H.Set<byte>(x, y, (byte)s[pixel]);
            }
        }

        //combina os 3 planos de pixels (H,S,V) novamente
        Cv2.Merge(planosHSV, imagemHSV);
        Mat imagemSaida = new Mat();
        //converte uma imagem HSV para RGB
        Cv2.CvtColor(imagemHSV, imagemSaida, ColorConversionCodes.HSV2BGR);
        return imagemSaida;
    }


}


















//using OpenCVForUnityUtils;
//using OpenCvSharp;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using UnityEngine;

//public class HaarTests : MonoBehaviour
//{

//    public String classifier = "aGest.xml";
//    double scale = 1.1;
//    bool error = false;
//    ScreenRecorder ARRecorder = null;
//    public Camera ARCamera = null;
//    public string folder;
//    public int captureWidth = 1920;
//    public int captureHeight = 1080;
//    public UnityEngine.UI.Text DebugOutText = null;
//    ScreenRecorder.Format ARCameraFormat = ScreenRecorder.Format.JPG;

//    Texture2D textureARCamera = null;
//    RenderTexture renderTextureARCamera = null;
//    public UnityEngine.UI.RawImage TargetImage = null;

//    public CascadeClassifier cascadeClassifier { get; private set; }

//    void Start()
//    {
//        ARRecorder = new ScreenRecorder()
//        {
//            Camera = ARCamera,
//            captureWidth = this.captureWidth,
//            captureHeight = this.captureHeight,
//            folder = this.folder,
//            format = this.ARCameraFormat
//        };
//        //window = new Window();
//        cascadeClassifier = new CascadeClassifier();
//        string path = Application.dataPath + "/" + classifier;
//        if (!System.IO.File.Exists(path))
//        {
//            throw new Exception("Não foi possível carregar o xml do classificador CascadeClassifier: "+ classifier);
//        }
//        else
//        {
//            cascadeClassifier.Load(path);
//        }



//    }

//    void Update()
//    {


//        try
//        {
//            if (textureARCamera != null)
//            {
//                if (TargetImage.texture != null)
//                    Destroy(TargetImage.texture);
//                Destroy(textureARCamera);
//                textureARCamera = null;
//                renderTextureARCamera.DiscardContents();
//                Destroy(renderTextureARCamera);
//            }
//            ARRecorder.Capture(out textureARCamera, out renderTextureARCamera);

//            using (Mat mat = textureARCamera.ToMat())
//            {



//                var objs = cascadeClassifier.DetectMultiScale(mat,scaleFactor: scale, flags: HaarDetectionType.DoCannyPruning);
//                //var objs = cascadeClassifier.DetectMultiScale(mat);
//                Point pt1, pt2;

//                DebugOutText.text = objs.Count() + " encontrados.";

//                foreach (var r in objs)
//                {
//                    pt1.X = r.X * (int)scale;
//                    pt2.X = (r.X + r.Width) * (int)scale;
//                    pt1.Y = r.Y * (int)scale;
//                    pt2.Y = (r.Y + r.Height) * (int)scale;
//                    Cv2.Rectangle(mat, pt1, pt2, Scalar.Green);

//                    DebugOutText.text += "P1  X: "+ pt1.X + " Y: "+ pt1.Y;

//                    DebugOutText.text += "\nP2  X: " + pt2.X + " Y: " + pt2.Y;

//                    DebugOutText.text += "\n\nR  H: " +r.Height + " W: " + r.Width;
//                }




//                if (TargetImage != null)
//                {
//                    Texture2D text = mat.ToTexture2D();

//                    TargetImage.texture = text;
//                    TargetImage.enabled = true;
//                }

//            }
//        }
//        catch (Exception ex)
//        {
//            DebugOutText.text = "err[1]:" + ex.ToString();
//            error = true;
//        }

//    }




//}

