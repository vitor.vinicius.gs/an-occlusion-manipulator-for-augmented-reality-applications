﻿using System;
using UnityEngine;
using OpenCVForUnityUtils;
using OpenCvSharp;
using System.Collections.Generic;

public class HandTracker : MonoBehaviour
{
    CascadeClassifier face_cascade;
    //Window window = null;
    Texture2D textureARCamera = null;
    RenderTexture renderTextureARCamera = null;
    public int captureWidth = 1920;
    public int captureHeight = 1080;
    ScreenRecorder.Format ARCameraFormat = ScreenRecorder.Format.JPG;
    public Camera ARCamera = null;
    ScreenRecorder ARRecorder = null;
    public string folder;
    public UnityEngine.UI.RawImage TargetImage = null;
    public UnityEngine.UI.Text DebugOutText = null;
    HandHeadDetection handHeadDetec = new HandHeadDetection();
    public bool UseRGB = true;
    public bool UseNormalizedRGB = true;
    public bool UseHSV = true;
    public OpenCvSharp.Rect boundingBox;
    public OpenCvSharp.Point center;
    public float boundingBoxAreaRatio;
    public float innerBoundingBoxAreaRatio;
    public float palmCircleAreaRatio;
    public int minAngle = -100;
    public int maxAngle = 200;
    public float boundingBoxHeightPerc = 0.15f;
    public int angleMax = 200;
    public int angleMin = 20;

    public bool drawDebugInfo = false;
    List<float> fps_list = new List<float>();

    void Start()
    {
        ARRecorder = new ScreenRecorder()
        {
            Camera = ARCamera,
            captureWidth = this.captureWidth,
            captureHeight = this.captureHeight,
            folder = this.folder,
            format = this.ARCameraFormat
        };
        //window = new Window();
        face_cascade = new CascadeClassifier();
        string path = Application.dataPath + "/haarcascade_frontalface_alt.xml";
        if (!System.IO.File.Exists(path))
        {
            throw new Exception("Não foi possível carregar o xml do classificador CascadeClassifier: haarcascade_frontalface_alt.xml");
        }
        else
        {
            face_cascade.Load(path);
        }



    }
    public void OnGUI()
    {
        //GUI.Box(new UnityEngine.Rect(0, 0, Screen.width / 2, Screen.height / 2), DebugOutText.text);
    }
    public Mat MakeDetection(Mat frame, out Point[][] contours, out OpenCvSharp.Rect boundingBox, out    OpenCvSharp.Point center, out List<OpenCvSharp.Point> fingertips, out List<Point> deptharr, out Point2f centerps,
    out float radips)
    {
        //OpenCvSharp.Rect recFace = new OpenCvSharp.Rect(0, 0, 0, 0); //handHeadDetec.facedetect(frame, face_cascade); // Find the face
        //Cv2.Rectangle(frame, recFace, new Scalar(0, 255, 0), 1, LineTypes.Link8, 0); // Draw rect on the face
        //recFace.X = recFace.Y = recFace.Top = recFace.Left = recFace.Width = recFace.Height = 0;
        ////recFace.TopLeft = new Point(0, 0); read only
        //recFace.Size = new Size(0, 0);
        ////recFace.Right = -1; read only
        ////recFace.BottomRight read only
        ////recFace.Bottom read only


        //int ymax; int ymin; int crmax; int crmin; int cbmax; int cbmin;
        //ymax = ymin = crmax = crmin = cbmax = cbmin = 0;
        //handHeadDetec.SkinColorModel(frame, recFace, ref ymax, ref ymin, ref crmax, ref crmin, ref cbmax, ref cbmin); // Filter & cleaning face area

        //handHeadDetec.HandDetection(ref frame, recFace, ymax, ymin, crmax, crmin, cbmax, cbmin, out contours, out boundingBox, out center,true); // Dectect hand
        handHeadDetec.UseHSV = this.UseHSV;
        handHeadDetec.UseRGB = this.UseRGB;
        handHeadDetec.UseNormalizedRGB = this.UseNormalizedRGB;
        handHeadDetec.inAngleMax = this.angleMax;
        handHeadDetec.inAngleMin = this.angleMin;
        handHeadDetec.maxAngle = this.maxAngle;
        handHeadDetec.minAngle = this.minAngle;
        handHeadDetec.boundingBoxHeightPerc = this.boundingBoxHeightPerc;

        frame = handHeadDetec.HandDetection(frame, out contours, out boundingBox, out center, out fingertips, out deptharr, out centerps, out radips, drawDebugInfo); // Dectect hand
        return frame;
    }
    bool error = false;
    void Update()
    {
        

        try
        {
            if (textureARCamera != null)
            {
                if(TargetImage.texture!=null)
                    Destroy(TargetImage.texture);
                Destroy(textureARCamera);
                textureARCamera = null;
                renderTextureARCamera.DiscardContents();
                Destroy(renderTextureARCamera);
            }
            ARRecorder.Capture(out textureARCamera, out renderTextureARCamera);


            using (Mat mat = textureARCamera.ToMat())
            {


               

                   

                    if (!face_cascade.Empty())
                    {
                        try
                        {
                            Point[][] contours = null;
                            List<Point> fingertips = null;
                            List<Point> deptharr = null;
                            Point2f centerps;
                            float radips;
                            if (mat != null)
                                using (Mat newMat = MakeDetection(mat, out contours, out boundingBox, out center, out fingertips, out deptharr, out centerps,
    out radips))// = mat)//
                            {
                                    fingertips.AddRange(deptharr); 
                                    OpenCvSharp.Rect innerBoundingBox = Cv2.BoundingRect(fingertips);
                                    newMat.Rectangle(innerBoundingBox, new Scalar(0, 0, 255), 2);
                                    boundingBoxAreaRatio = ((float)(boundingBox.Height * boundingBox.Width))/(mat.Rows*mat.Cols);
                                    innerBoundingBoxAreaRatio = ((float)(innerBoundingBox.Height * innerBoundingBox.Width)) / (mat.Rows * mat.Cols);
                                    palmCircleAreaRatio = (float)(Math.PI * Math.Pow(radips,2))/ (mat.Rows * mat.Cols);

                                

                                if (TargetImage != null)
                                {
                                    Texture2D text = newMat.ToTexture2D();
                                   
                                    TargetImage.texture = text;
                                    TargetImage.enabled = true;
                                }
                            }
                            contours = null;
                        }
                        catch (Exception ex)
                        {
                            TargetImage.enabled = false;
                            error = true;
                            DebugOutText.text = "err[2]:" + ex.Message;
                        }

                    }




                    //window.ShowImage(mat);

                


            }

        }
        catch (Exception ex)
        {
            DebugOutText.text = "err[1]:" + ex.ToString();
            error = true;
        }
        //feedback = !feedback;
        //if (!error)
        //    DebugOutText.text = "Working" + (feedback ? ". " + 1/Time.deltaTime : "");


        if (!error){

            float fps = 1 / Time.deltaTime;

            fps_list.Insert(0,fps);

            float fpsSum = 0;
            foreach (var fpsI in fps_list)
                fpsSum += fpsI;

            float avgFps = fpsSum / (float)fps_list.Count;

            if (fps_list.Count > 500)
            {
                for(int i = 499; i< fps_list.Count; i++)
                {
                    fps_list.RemoveAt(i);
                }
                
            }


            DebugOutText.text = "Working" + ". " + avgFps+ " at "+ SystemInfo.graphicsDeviceName;
        }
    }

}



