﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class TargetInfo : MonoBehaviour {

    public GameObject VirtualObject;
    public GameObject ARCamera;
    
    public HandTracker tracker;

    public OcclusionAnalyzer OclusionAnalyzer;

    public float DistanceToVirtualObject;
    public float RealObjectImgRatio;

    public float EstimatedDistanceToRealObject;
    public float EstimationConstant = 1;

    public bool take = false;

    public Text TargetInfoOutput = null;

    // Use this for initialization
    void Start () {
      
    }

    public void Take() {
        take = true;
    }

    public void OnGUI()
    {
     if(TargetInfoOutput!=null)
        {
            TargetInfoOutput.text = string.Format("Real Obj Dist: {0}\tVirtual Obj Dist: {1}\tReal Obj Area Ratio:{2}", System.Math.Round(EstimatedDistanceToRealObject, 2).ToString().PadRight(5).PadLeft(5), System.Math.Round(DistanceToVirtualObject,2).ToString().PadRight(5).PadLeft(5), System.Math.Round(tracker.boundingBoxAreaRatio,2).ToString().PadRight(5).PadLeft(5));
        }
    }

    // Update is called once per frame
    public float lastRatio = 0;
    void Update () {
        
        DistanceToVirtualObject = (ARCamera.transform.position - VirtualObject.transform.position).magnitude;
        RealObjectImgRatio = 1/(tracker.innerBoundingBoxAreaRatio + lastRatio)/(lastRatio == 0?1:2);
        lastRatio = RealObjectImgRatio;
        EstimatedDistanceToRealObject = EstimationConstant * RealObjectImgRatio;
        
        

        if (take)
        {
            take = false;
            EstimationConstant  = DistanceToVirtualObject/RealObjectImgRatio ;

        }

        OclusionAnalyzer.active = EstimatedDistanceToRealObject <= DistanceToVirtualObject;




    }

   
}
