﻿using UnityEngine;
using Vuforia;

public class DirSwap : MonoBehaviour
{

    

    void OnGUI()
    {
        if (GUI.Button(new Rect(50, 50, 200, 50), "Swap Camera"))
        {
            CameraDevice.CameraDirection currentDir = CameraDevice.Instance.GetCameraDirection();
            if (currentDir == CameraDevice.CameraDirection.CAMERA_BACK || currentDir == CameraDevice.CameraDirection.CAMERA_DEFAULT)
                RestartCamera(CameraDevice.CameraDirection.CAMERA_FRONT, true);
            else
                RestartCamera(CameraDevice.CameraDirection.CAMERA_BACK, false);
        }

       
    }

    private void RestartCamera(CameraDevice.CameraDirection newDir, bool mirror)
    {
        CameraDevice.Instance.Stop();
        CameraDevice.Instance.Deinit();

        CameraDevice.Instance.Init(newDir);


        CameraDevice.Instance.Start();
    }
}