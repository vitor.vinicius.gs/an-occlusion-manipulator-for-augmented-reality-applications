# An Occlusion Manipulator for augmented reality applications

The treatment of occlusion is an important factor to avoid visual incoherence in augmented reality (AR) systems. This feature allows real and virtual elements present in the scene to overlap each other according to their distances from the camera. This work proposes an approach for treatment of occlusion in AR using only an RGB camera and image processing techniques. The element to be displayed in the foreground is estimated from the calculation of the proportion it occupies in the frame in relation to the virtual object. The initial ratio of both is obtained in a calibration offline process. A skin tone-based segmentation algorithm is used to extract the real element so that it can be superimposed on the virtual in the frames in which it is in the foreground in the scene.


***Source code and papers coming soon***